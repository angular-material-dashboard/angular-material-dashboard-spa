/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSpa', [ //
    'ngMaterialDashboard'//
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSpa')
/**
 * 
 */
.config(function($routeProvider) {
	$routeProvider //
	.when('/spas', {
		controller : 'amdSpasCtrl',
		controllerAs: 'ctrl',
		templateUrl : 'views/amd-spas.html',
		navigate : true,
		protect: true,
		name : 'spas',
		icon : 'apps',
		groups : [ 'spa' ],
	}) //
	.when('/spas/upload', {
		controller : 'amdSpaUploadCtrl',
		templateUrl : 'views/amd-spa-upload.html',
		navigate : true,
		protect: true,
		name : 'Upload spa',
		icon : 'file_upload',
		groups : [ 'spa' ],
	}) //
	.when('/spas/repository', {
		controller : 'amdReposiotrySpasCtrl',
		controllerAs: 'ctrl',
		templateUrl : 'views/amd-rspas.html',
		navigate : true,
		protect: true,
		name : 'Repository',
		icon : 'cloud_upload',
		groups : [ 'spa' ],
	}) //
	.when('/spas/repository/:spaId', {
		controller : 'amdRepositorySpaCtrl',
		templateUrl : 'views/amd-rspa.html',
		protect: true,
	}) //
	.when('/spas/:spaId', {
		controller : 'amdSpaCtrl',
		templateUrl : 'views/amd-spa.html',
		protect: true,
	});
});
/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSpa')

/**
 * 
 */
.controller('amdRepositorySpaCtrl', function($scope, $routeParams, $tenant) {

	/**
	 * Load the spa
	 */
	function load() {
		$scope.working = $tenant.getRepositorySpa($routeParams.spaId)//
		.then(function(spa){
			$scope.spa = spa;
			return $scope.spa.getPossibleTransitions();
		})//
		.then(function(states){
			$scope.states = states.items;
		})//
		.finally(function(){
			$scope.working = false;
		});
		return $scope.working;
	}

	/**
	 * Go to the new state.
	 */
	function gotoState(state){
		if($scope.working){
			return;
		}
		// Load data for state
		return $scope.working = $scope.spa.putTransition(state)//
		.then(function(){
			toast('Process done successfully.');
		}, function(){
            alert('Process failed.');
		})//
		.finally(function(){
		    $scope.working = false;
		});
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
	 * شده است.
	 */
	$scope.gotoState = gotoState;

	load();
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSpa')

/**
 * 
 */
.controller('amdReposiotrySpasCtrl', function ($scope, $controller, $navigator, $tenant) {

    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope: $scope
    }));

    /*
     * Overried the function
     */
    this.getModelSchema = function () {
        return $tenant.repositorySpaSchema();
    };

    // get spas
    this.getModels = function (parameterQuery) {
        return $tenant.getRepositorySpas(parameterQuery);
    };

    // get a spa
    this.getModel = function (id) {
        return $tenant.getRepositorySpa(id);
    };

    // delete spa
    this.deleteModel = function (item) {
        return item.delete();
    };

    this.init({
        eventType: '/spas/repository'
    });

    /**
     * add an spa
     */
    this.addSpa = function () {
        $navigator.openPage('spas/upload');
    };

    /**
     * Add an spa from repository
     */
    this.addSpaFromRepo = function () {
        $navigator.openPage('spas/repository');
    };

    $scope.sortKeys = ['id', 'license', 'name', 'title', 'creation_dtime'];

    this.addActions([]);
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSpa')

/**
 * 
 */
.controller('amdSpaUploadCtrl', function($scope, $tenant, $navigator) {
	var ctrl = {
			state : 'relax',
			uploading : false
	};

	/**
	 * Upload an spa file.
	 * 
	 * @returns
	 */
	function fileSelected(element) {
		ctrl.uploading = true;
		return $tenant.putSpa(element[0].lfFile)//
		.then(function(spa) {
			toast('Application is installed successfully.');
			return $navigator.openPage('spas/' + spa.id);
		}, function(ex) {
			alert('Fail to install app: ' + ex.data.message);
		})
		.finally(function(){
			ctrl.uploading = false;
		});
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.ctrl = ctrl;
	$scope.upload = fileSelected;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialDashboardSpa')

/**
 * @ngdoc controller
 * @name SpaCtrl
 * @description # SpaCtrl Controller of the ngMaterialDashboard
 */
.controller('amdSpaCtrl', function ($scope, $tenant, $routeParams, $location, $http, $navigator) {
	/**
	 * Controller data
	 */
	$scope.ctrl = {
		state: 'relax'
	};
	var ctrl = $scope.ctrl;

	/**
	 * Update current local changes into the backend
	 */
	function update(){
		ctrl.state = 'working';
		return $scope.spa.update()
		.then(function(){
			alert('update is successfully.');
		}, function(error){
			alert('fail to update.');
		})//
		.finally(function(){
			ctrl.state = 'relax';
		});
	}
	
	/**
	 * Update name of the spa
	 */
	function updateName(){
		return confirm("Name of an spa directly effect into the SEO and cause fail in update process. Change spa name?")//
		.then(function(){
			ctrl.state = 'working';
			return $scope.spa.update();//
		})//
		.finally(function(){
			ctrl.state = 'relax';
		});
	}
	
	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove() {
		return confirm("delete spa " + $scope.spa.id +"?")//
		.then(function(){
			ctrl.state = 'working';
			return $scope.spa.delete();//
		})//
		.then(function(){
			$location.path('/spas');
		}, function(error){
			alert('fail to delete spa:' + error.message);
		})//
		.finally(function(){
			ctrl.state = 'relax';
		});
	}

	/**
	 * Load the spa
	 */
	function load() {
		ctrl.state = 'working';
		return $tenant.getSpa($routeParams.spaId)//
		.then(function(spa){
			$scope.spa = spa;
			return $scope.spa.getPossibleTransitions();
		}, function(error){
		    if(error.status === 404){
		        $navigator.openPage('spas');
		    }
			ctrl.error = error;
		})//
		.then(function(transList){
			ctrl.error = null;
			$scope.transitions = transList;
		})//
		.finally(function(){
			ctrl.state = 'relax';
		});
	}

	/**
	 * تنظیم به عنوان نرم افزار پیش فرض
	 * 
	 * این نرم افزار را به عنوان نرم افزار پیش فرض تعیین می‌کند.
	 * 
	 * @returns
	 */
	function setDefault(){
		ctrl.state = 'working';
		return $tenant.getSetting('spa.default')//
		// TODO: maso, 2018: check if not exist
		.then(function(setting){
			setting.value = $scope.spa.name;
			return setting.update();
		})//
		.then(function(){
			toast('Default SPA is changed.');
		},function(error){
			alert('Fail to set default spa:'+error.message);
		})//
		.finally(function(){
			ctrl.state = 'relax';
		});
	}

	/**
	 * Add given transition to SPA. In other word, do some transaction on SPA.
	 */
	function addTransition(state){
		if(ctrl.state !== 'relax'){
			return;
		}
		ctrl.state = 'working';
		// Load data for state
		var data = {}; 
//		$http.put('/api/spa/'+$scope.spa.id+'/states/'+state.id, data)//
		$scope.spa.putTransition(state)
		.then(function(){
			return load();
		}, function(error){
			ctrl.error = error;
			alert('Process failed. ' + error.message);
		})//
		.finally(function(){
			ctrl.state = 'relax';
		});
	}
	
	function deleteSpa (){
        if(ctrl.state !== 'relax'){
            return;
        }
        ctrl.state = 'working';
        confirm('Delete the SPA?')
        .then(function(){
            $scope.spa.delete()
            .then(function(){
                $navigator.openPage('spas');
            }, function(error){
                ctrl.error = error;
                alert('Failed to delete: ' + error.message);
            })//
        })
        .finally(function(){
            ctrl.state = 'relax';
        });
	}

	// Load state
	$scope.setDefault = setDefault;
	$scope.remove = remove;
	$scope.update = update;
	$scope.updateName = updateName;
	$scope.addTransition = addTransition;
	$scope.deleteSpa = deleteSpa;
	load();
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSpa')

/**
 * @ngdoc function
 * @name ngMaterialDashboard.controller:GroupsCtrl
 * @description # GroupsCtrl Controller of the ngMaterialDashboard
 */
.controller('amdSpasCtrl', function ($scope, $tenant, $navigator, $controller) {

    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope: $scope
    }));

    /*
     * Overried the function
     */
    this.getModelSchema = function () {
        return $tenant.spaSchema();
    };

    // get spas
    this.getModels = function (parameterQuery) {
        return $tenant.getSpas(parameterQuery);
    };

    // get a spa
    this.getModel = function (id) {
        return $tenant.getSpa(id);
    };

    // delete spa
    this.deleteModel = function (item) {
        return item.delete();
    };

    this.init({
        eventType: '/spas'
    });

    /**
     * add an spa
     */
    this.addSpa = function () {
        $navigator.openPage('spas/upload');
    };

    /**
     * Add an spa from repository
     */
    this.addSpaFromRepo = function () {
        $navigator.openPage('spas/repository');
    };

    this.sortKeys = ['id', 'creation_dtime'];

    var ctrl = this;
    this.addActions([{
        title: 'Upload spa',
        icon: 'add',
        action: function () {
            ctrl.addSpa();
        }
    }, {
        title: 'Add spa from repository',
        icon: 'add',
        action: function () {
            ctrl.addSpaFromRepo();
        }
    }]);
});

//Note: Hadi 1397-07-06: Use RepositorySpa factory in seen-tenant or Spa factory in seen-marketplace instead of this factory.

///*
// * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialDashboardSpa')
///**
// * @ngdoc factory
// * @memberof ngMaterialDashboardSpa
// * @name MarketSpa
// * @description 
// * 
// * # spa data model
// * 
// */
//.factory('MarketSpa', function(PObject, $pluf) {
//	var spa = function() {
//		PObject.apply(this, arguments);
//	};
//	spa.prototype = new PObject();
//
//	/**
//	 * Delete the model
//	 * 
//	 * @memberof MarketSpa
//	 * @return {promise<MarketSpa>} 
//	 */
//	spa.prototype.delete = $pluf.createDelete({
//		method : 'DELETE',
//		url : '/api/repository/:id'
//	});
//
//	/**
//	 * Update the model
//	 * 
//	 * @memberof MarketSpa
//	 * @return {promise<MarketSpa>} 
//	 */
//	spa.prototype.update =  $pluf.createUpdate({
//		method : 'POST',
//		url : '/api/repository/:id',
//	});
//	
//
//	/**
//	 * List of all states
//	 */
//	spa.prototype.states = $pluf.get({
//		url: '/api/repository/:id/states/find'
//	});
//
//	/**
//	 * 
//	 * <pre><code>
//	 * 	spa.gotState(data, state).then(function(result){});
//	 * </code></pre>
//	 */
//	spa.prototype.gotoState = $pluf.put({
//		url: '/api/repository/:id/states/{id}'
//	});
//
//	return spa;
//});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardSpa')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($navigator) {
	$navigator
	.newGroup({
		id: 'spa',
		title: 'Spa management',
		description: 'A module of dashboard to manage spa.',
		icon: 'apps',
		hidden: '!app.user.tenant_owner',
		priority: 5
	});
});
//Note: Hadi 1397-07-06: Use $marketplace in seen-marketplace instead of this service.

///*
// * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialDashboardSpa')
///**
// * @ngdoc service
// * @name $marketplace
// * @memberof ngMaterialDashboardSpa
// * @description 
// * 
// * # marketplace
// * 
// */
//.service('$marketplace', function($http, PObjectCache, $pluf, MarketSpa) {
//
//	var _spas = new PObjectCache(function(data) {
//		return new MarketSpa(data);
//	});
//
//
//	/**
//	 * List spas
//	 * 
//	 * @memberof $marketplace
//	 * @return {permision(PaginatedPage<MarketSpa>)} list of spas
//	 */
//	this.spas = $pluf.createFind({
//		url : '/api/repository/find',
//	}, _spas);
//
//	/**
//	 * Get an spa
//	 * 
//	 * @memberof $marketplace
//	 * @param {object} id of the spa
//	 * @return {promise<marketplacespa>} the spa
//	 */
//	this.spa = $pluf.createGet({
//		method: 'GET',
//		url : '/api/repository/{id}',
//	}, _spas);
//
//
//	/**
//	 * Creates new spa
//	 * 
//	 * @memberof $marketplace
//	 * @param {Struct} data of an spa
//	 * @return {promise<marketplacespa>} created one
//	 */
//	this.newSpa = $pluf.post({
//		url : '/api/repository/new',
//	}, _spas);
//
//});
angular.module('ngMaterialDashboardSpa').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-rspa.html',
    "<md-content layout=column mb-preloading=working flex> <div layout=row layout-padding flex> <img width=150px height=150px ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <table class=\"md-table md-table-description\"> <tr> <td translate>Title</td> <td>{{spa.title}}</td> </tr> <tr> <td translate>Description</td> <td>{{spa.description}}</td> </tr> <tr> <td translate>Name</td> <td>{{spa.name}}</td> </tr> <tr> <td translate>Version</td> <td>{{spa.version}}</td> </tr> <tr> <td translate>License</td> <td>{{spa.license}}</td> </tr> <tr> <td translate>Home page</td> <td>{{spa.homepage}}</td> </tr> </table> </div> <div layout=row> <md-button class=md-raised ng-href=spas/repository> <span translate>Back to list</span> </md-button> <span flex></span> <md-button class=\"md-primary md-raised\" data-ng-repeat=\"state in states\" data-ng-click=gotoState(state) data-ng-show={{state.visible}}> <span translate>{{state.title || state.id}}</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-rspas.html',
    "<div mb-preloading=ctrl.loading layout=column flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list ng-if=ctrl.items.length flex> <md-list-item ng-repeat=\"spa in ctrl.items\" class=md-3-line ng-href=spas/repository/{{spa.id}}> <img ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <div class=md-list-item-text layout=column> <h3>{{spa.title}}</h3> <h4>{{spa.name}} - {{spa.version}}</h4> <p>{{spa.description}}</p> </div> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-spa-upload.html',
    "<md-content mb-preloading=ctrl.uploading layout=column flex> <lf-ng-md-file-input lf-files=files accept=application/zip progress drag> </lf-ng-md-file-input> <div layout=row> <span flex></span> <md-button class=\"md-primary md-raised\" ng-click=upload(files)> <span translate>Upload</span> </md-button> <md-button class=md-raised ng-href=spas translate> <span translate>Cancel</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-spa.html',
    "<md-content layout=column mb-preloading=\"ctrl.state==='working'\" flex> <div layout=row layout-padding flex> <img width=150px height=150px ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <table class=\"md-table md-table-description\"> <tr> <td translate>Title</td> <td> <am-wb-inline ng-model=spa.title am-wb-inline-type=text am-wb-inline-label=\"Content name\" am-wb-inline-enable=true am-wb-inline-on-save=update()> {{spa.title}} </am-wb-inline> </td> </tr> <tr> <td translate>Description</td> <td> <am-wb-inline ng-model=spa.description am-wb-inline-type=textarea am-wb-inline-label=\"Content name\" am-wb-inline-enable=true am-wb-inline-on-save=update()> {{spa.description|| '...'}} </am-wb-inline> </td> </tr> <tr> <td translate>Name</td> <td> <am-wb-inline ng-model=spa.name am-wb-inline-type=text am-wb-inline-label=\"Content name\" am-wb-inline-enable=true am-wb-inline-on-save=updateName()> {{spa.name}} </am-wb-inline> </td> </tr> <tr> <td translate>Version</td> <td>{{spa.version}}</td> </tr> <tr> <td translate>License</td> <td>{{spa.license}}</td> </tr> <tr> <td translate>Home page</td> <td>{{spa.homepage}}</td> </tr> <tr ng-if=\"spa.last_version !== spa.version\"> <td translate>New version</td> <td>{{spa.last_version}}</td> </tr> </table> </div> <div layout=row> <md-button class=md-raised data-ng-repeat=\"trans in transitions.items\" data-ng-click=addTransition(trans) data-ng-show={{trans.visible}}> <span translate>{{trans.title|| trans.id}}</span> </md-button> <span flex></span> <md-button class=md-raised target=_blank ng-href=\"/{{spa.name}}/\"> <span translate>Run</span> </md-button> <md-button class=md-raised ng-click=deleteSpa()> <span translate>Delete</span> </md-button> <md-button class=md-raised ng-click=setDefault()> <span translate>Set default</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-spas.html',
    "<div mb-preloading=ctrl.loading layout=column flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <md-list ng-if=\"ctrl.items.length > 0\" flex> <md-list-item ng-repeat=\"spa in ctrl.items\" class=md-3-line ng-href=spas/{{spa.id}}> <img ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <div class=md-list-item-text layout=column> <h3>{{spa.title}}</h3> <h4>{{spa.name}}-{{spa.version}}</h4> <p>{{spa.description}}</p> </div> </md-list-item> </md-list> <div layout=column ng-if=\"ctrl.state != 'busy' && ctrl.items.length === 0\"> <h3 style=\"text-align: center\" translate>Item not found</h3> <div layout=row layout-align=\"center center\"> <md-button ng-href=spas/upload> <wb-icon>add</wb-icon> <span translate>Upload</span> </md-button> <md-button ng-href=spas/repository> <wb-icon>apps</wb-icon> <span translate>Install from repository</span> </md-button> </div> </div> </md-content> </div>"
  );


  $templateCache.put('views/amd-tenant.html',
    ""
  );

}]);
