angular.module('ngMaterialDashboardSpa').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-rspa.html',
    "<md-content layout=column mb-preloading=working flex> <div layout=row layout-padding flex> <img width=150px height=150px ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <table class=\"md-table md-table-description\"> <tr> <td translate>Title</td> <td>{{spa.title}}</td> </tr> <tr> <td translate>Description</td> <td>{{spa.description}}</td> </tr> <tr> <td translate>Name</td> <td>{{spa.name}}</td> </tr> <tr> <td translate>Version</td> <td>{{spa.version}}</td> </tr> <tr> <td translate>License</td> <td>{{spa.license}}</td> </tr> <tr> <td translate>Home page</td> <td>{{spa.homepage}}</td> </tr> </table> </div> <div layout=row> <md-button class=md-raised ng-href=spas/repository> <span translate>Back to list</span> </md-button> <span flex></span> <md-button class=\"md-primary md-raised\" data-ng-repeat=\"state in states\" data-ng-click=gotoState(state) data-ng-show={{state.visible}}> <span translate>{{state.title || state.id}}</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-rspas.html',
    "<div mb-preloading=ctrl.loading layout=column flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list ng-if=ctrl.items.length flex> <md-list-item ng-repeat=\"spa in ctrl.items\" class=md-3-line ng-href=spas/repository/{{spa.id}}> <img ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <div class=md-list-item-text layout=column> <h3>{{spa.title}}</h3> <h4>{{spa.name}} - {{spa.version}}</h4> <p>{{spa.description}}</p> </div> </md-list-item> </md-list> </md-content> </div>"
  );


  $templateCache.put('views/amd-spa-upload.html',
    "<md-content mb-preloading=ctrl.uploading layout=column flex> <lf-ng-md-file-input lf-files=files accept=application/zip progress drag> </lf-ng-md-file-input> <div layout=row> <span flex></span> <md-button class=\"md-primary md-raised\" ng-click=upload(files)> <span translate>Upload</span> </md-button> <md-button class=md-raised ng-href=spas translate> <span translate>Cancel</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-spa.html',
    "<md-content layout=column mb-preloading=\"ctrl.state==='working'\" flex> <div layout=row layout-padding flex> <img width=150px height=150px ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <table class=\"md-table md-table-description\"> <tr> <td translate>Title</td> <td> <am-wb-inline ng-model=spa.title am-wb-inline-type=text am-wb-inline-label=\"Content name\" am-wb-inline-enable=true am-wb-inline-on-save=update()> {{spa.title}} </am-wb-inline> </td> </tr> <tr> <td translate>Description</td> <td> <am-wb-inline ng-model=spa.description am-wb-inline-type=textarea am-wb-inline-label=\"Content name\" am-wb-inline-enable=true am-wb-inline-on-save=update()> {{spa.description|| '...'}} </am-wb-inline> </td> </tr> <tr> <td translate>Name</td> <td> <am-wb-inline ng-model=spa.name am-wb-inline-type=text am-wb-inline-label=\"Content name\" am-wb-inline-enable=true am-wb-inline-on-save=updateName()> {{spa.name}} </am-wb-inline> </td> </tr> <tr> <td translate>Version</td> <td>{{spa.version}}</td> </tr> <tr> <td translate>License</td> <td>{{spa.license}}</td> </tr> <tr> <td translate>Home page</td> <td>{{spa.homepage}}</td> </tr> <tr ng-if=\"spa.last_version !== spa.version\"> <td translate>New version</td> <td>{{spa.last_version}}</td> </tr> </table> </div> <div layout=row> <md-button class=md-raised data-ng-repeat=\"trans in transitions.items\" data-ng-click=addTransition(trans) data-ng-show={{trans.visible}}> <span translate>{{trans.title|| trans.id}}</span> </md-button> <span flex></span> <md-button class=md-raised target=_blank ng-href=\"/{{spa.name}}/\"> <span translate>Run</span> </md-button> <md-button class=md-raised ng-click=deleteSpa()> <span translate>Delete</span> </md-button> <md-button class=md-raised ng-click=setDefault()> <span translate>Set default</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-spas.html',
    "<div mb-preloading=ctrl.loading layout=column flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <md-list ng-if=\"ctrl.items.length > 0\" flex> <md-list-item ng-repeat=\"spa in ctrl.items\" class=md-3-line ng-href=spas/{{spa.id}}> <img ng-src=/{{spa.name}}/images/logo.svg class=\"md-avatar\"> <div class=md-list-item-text layout=column> <h3>{{spa.title}}</h3> <h4>{{spa.name}}-{{spa.version}}</h4> <p>{{spa.description}}</p> </div> </md-list-item> </md-list> <div layout=column ng-if=\"ctrl.state != 'busy' && ctrl.items.length === 0\"> <h3 style=\"text-align: center\" translate>Item not found</h3> <div layout=row layout-align=\"center center\"> <md-button ng-href=spas/upload> <wb-icon>add</wb-icon> <span translate>Upload</span> </md-button> <md-button ng-href=spas/repository> <wb-icon>apps</wb-icon> <span translate>Install from repository</span> </md-button> </div> </div> </md-content> </div>"
  );


  $templateCache.put('views/amd-tenant.html',
    ""
  );

}]);
